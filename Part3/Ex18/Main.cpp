#include <iostream>
using namespace std;



int factorialSecond(int fact)
{	
	int tmpResult = 1;
	int i = 0;
	while(i < fact)
	{
		i++;
		tmpResult *= i;
	}	
	return tmpResult;
}

int factorialFirst(int fact)
{
	int tmpResult = 1;
	for(int i = 1; i < fact+1; i++)
		tmpResult *= i;
	return tmpResult;
}

int main()
{
	cout << factorialFirst(5) << endl;
	cout << factorialSecond(10) << endl;
}
#include <iostream>
#include <string>
using namespace std;


string revString(string str1)
{
	int tmpLength = str1.size();
	int tmpPairs = tmpLength/2;
	char tmpStr;
	int j = tmpLength;
	for(int i = 0; i < tmpPairs; i++)
	{		
		j--;
		tmpStr = str1[i];
		str1[i] = str1[j];
		str1[j] = tmpStr;
	}
	return str1;	
}

char *revChar(char *str1) 
{
	int length = strlen(str1);
	char *pNewStr = new char[length+1];
	int j = 0;
	for(int i = length; i >= 0; i--)
	{
		if(str1[i] != '\0')
		{
			pNewStr[j] = str1[i]; 
			j++;
		}
	}
	pNewStr[j] = '\0';
	return pNewStr;
}


int main()
{
	char *a = "Hello";
	cout << revChar(a) << endl;	

}
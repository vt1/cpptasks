#include <iostream>
using namespace std;


//Demonstrate sample, when it has sense of using declaring name into initializer.

int main()
{
	char tmpVarForFilling;
	cout << "_" << " ";
	for(int i = 65; i < 68; i++)
	{
		tmpVarForFilling = i;
		cout << tmpVarForFilling << " ";		
	}	
	cout << endl;
	for(int j = 1; j < 4; j++)	
	{
		cout << j << " " << "_" << " " << "_" << " " << "_" << '\n';		
	}		
}
#include <iostream>
using namespace std;


void fn(int *a, int *b)
{
	cout << "before a : " << *a << endl;
	cout << "before b : " << *b << endl;
	int c = *a;
	*a = *b;
	*b = c;
	cout << "after a : " << *a << endl;
	cout << "after b : " << *b << endl;
	
}
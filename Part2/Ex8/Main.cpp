#include <iostream>
#include <string>

using namespace std;


//use an array 
//of char for the name of the month and an array of type int for the amount of days;



int main()
{
	int days[] = {30,31,29,28,29,30,31,30,31,29,30,31};

	const int sizeArray = 12;
	char *month[sizeArray] = {"January" , "February" , "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" } ;

	for(int i = 0; i<sizeArray; i++)
		cout << "Month:" << month[i] << "       Days: " << days[i] << endl;	
}
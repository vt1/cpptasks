#include <iostream>
#include "Fn.h"


//Define the function 
//f(char), g(char&) and h(const char&)
//Call them with arguments 'a', 49, 3300, c , uc and se, where c is char, uc - unsigned char, se - signed char.
//Which of these calls are available? In which calls a compiler will create temporary variables?



int main()
{
	char one = 'a';
	char two = 49;
	char three = 3300;
	char c;
	unsigned char uc;
	signed char sc;

	f(one);
	f(two);
	f(three);
	f(c);
	f(uc);
	f(sc);
	//All calls are available.

	g(&one);
	g(&two);
	g(&three);
	g(&c);
	//g(&uc); //error
	//g(&sc); //error
	//Almost all calls are available except unsigned and signed char.

	h(&one);
	h(&two);
	h(&three);
	h(&c);
	//h(&uc); //error
	//h(&sc); //error
	//Almost all calls are available except unsigned and signed char.
}
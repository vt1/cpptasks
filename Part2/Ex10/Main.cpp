#include <iostream>
#include <string>

using namespace std;


//Define an array of strings containing the names of the months. print 
//these lines. Print pass this array to a function.

void printMonths(string arrayMonth[], int sizeArray);

int main()
{
	const int sizeArray = 12;	
	string arrayMonth[sizeArray] = {"January" , "February" , "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" } ;
	printMonths(arrayMonth, sizeArray);
}

void printMonths(string arrayMonth[], int sizeArray)
{
	for(int i = 0; i < sizeArray; i++)
		cout << "Month:" << arrayMonth[i] << endl;
}



#include <iostream>
using namespace std;



//What is the size of array "str" in following sample?
//char str[] = "a short string";
//What is the length of string "a short string"?


int getArrLength(char array[]); //need to declare before main

int main()
{
	char str[] = "a short string";	
	const int sizeArray = sizeof(str);
	int numberOfElementsArr = getArrLength(str);

	cout << "Size is: " << sizeArray << endl;
	cout << "Length is: " << numberOfElementsArr << endl;
}

int getArrLength(char array[])
{
	int lengthArray = NULL;
	for(int i = 0; array[i]!= 0; i++)
	{
		lengthArray++;				
	}
	lengthArray--; // because array begins from 0(index)	
	return lengthArray;
}
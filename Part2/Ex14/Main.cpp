#include <iostream>
#include <string>
#include <ostream>
#include <ctime>

using namespace std;



//Define struct Date for keeping dates. Write functions for reading dates from enter thread, for outputing dates, and for init them.



struct Date
{
	int day;
	int month;
	int year;
};

Date *initDate()
{
	Date *pDate = new Date();

	pDate->day = 13;
	pDate->month = 5;
	pDate->year = 2014;

	return pDate;	
}

void setDate(Date *pDate)
{	
	cout << "Enter day:" << endl;
	cin >> pDate->day;
	cout << "Enter month:" << endl;
	cin >> pDate->month;
	cout << "Enter year:" << endl;
	cin >> pDate->year;
}

void getDate(Date *pDate)
{
	cout << "Day:" << pDate->day << " Month:" << pDate->month << " Year:" << pDate->year << endl;
}

int main()
{	
	Date *pNewDate = initDate(); 
	setDate(pNewDate);
	getDate(pNewDate);
}

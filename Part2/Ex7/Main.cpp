#include <iostream>
#include <string>

using namespace std;


//Define table names month and how many days are there. Display it.
//Do it twice: 2. Use array structure, which contains names of months and days.
//1.Use array elements type (char) for month names and array type (int) for days.



struct DayAndMonth
{
	int day;
	string month;
};

void initArray(DayAndMonth dayAndMonth[]);

int main()
{
	const int sizeArray = 12;
	DayAndMonth dayAndMonth[sizeArray];
	initArray(dayAndMonth);


	for(int i = 0; i<sizeArray; i++)	
	{
		cout <<	"Month: " << dayAndMonth[i].month << endl;;
		cout << "Day: " << dayAndMonth[i].day << endl;
	}
	

	
}

void initArray(DayAndMonth dayAndMonth[])
{
	dayAndMonth[0].day = 31;
	dayAndMonth[0].month = "January";

	dayAndMonth[1].day = 28;
	dayAndMonth[1].month = "February";

	dayAndMonth[2].day = 31;
	dayAndMonth[2].month = "March";

	dayAndMonth[3].day = 30;
	dayAndMonth[3].month = "April";

	dayAndMonth[4].day = 31;
	dayAndMonth[4].month = "May";

	dayAndMonth[5].day = 30;
	dayAndMonth[5].month = "June";

	dayAndMonth[6].day = 31;
	dayAndMonth[6].month = "July";

	dayAndMonth[7].day = 31;
	dayAndMonth[7].month = "August";

	dayAndMonth[8].day = 30;
	dayAndMonth[8].month = "September";

	dayAndMonth[9].day = 31;
	dayAndMonth[9].month = "October";

	dayAndMonth[10].day = 30;
	dayAndMonth[10].month = "November";

	dayAndMonth[11].day = 31;
	dayAndMonth[11].month = "December";
}
